# Matrix Manager

This script is intended to completely streamline the process of spinning up a homelab'd (or VPS'd) Matrix server for personal use.

It automates the adding/deleting of bridges as `podman` containers and `systemd` services.
