#! /usr/bin/env python
# ChatGPT generated almost all of the below, full disclosure

import os
import subprocess
import yaml
import argparse
from types import SimpleNamespace

def log_info(message):
    print(f"\033[92mINFO:\033[0m {message}")

def log_error(message):
    print(f"\033[91mERR:\033[0m {message}")

def handle_command(cmd_list):
    log_info("Attempting to run command '"+' '.join(cmd_list)+"'")
    cmd = subprocess.run(cmd_list, check=True)

def get_config():
    config_file = os.path.join(os.getenv('HOME'), 'matrixmgr.yaml')
    if not os.path.isfile(config_file):
        with open(config_file, 'w') as file:
            log_error("Did not find 'matrixmgr.yaml' in the current working directory")
            log_info("Creating template/sample config file in CWD")
            file.write(
"""bridges:
  ghcr.io/matrix-org/synapse: latest # Do NOT remove this line! Modify the tag if you desire.
  another_image: tag
"""
            )
            log_info("Created template/sample config file.")
            exit(1)

    with open(config_file, 'r') as file:
        config = yaml.safe_load(file)

    # Validate config

    if 'postgres_tag' not in config:
        log_error("Missing the postgres_tag key in the config YAML")
        exit(2)

    if 'postgres_initial_password' not in config or config['postgres_initial_password'] == 'matrixiscool!':
        log_error("Either you didn't set the 'postgres_initial_password' key in the YAML or you didn't change the default password.")
        exit(2)

    if 'bridges' not in config:
        log_error("Bridges not defined in your config.")
        exit(2)

    if 'ghcr.io/matrix-org/synapse' not in config['bridges']:
        log_error("synapse is missing from your list of bridges.")
        exit(2)

    return config


def first_time_setup():
    config = get_config()

    # Extract images
    image_dict = config['bridges']

    # Ensure synapse image is included
    if "ghcr.io/matrix-org/synapse" not in image_dict:
        image_dict["ghcr.io/matrix-org/synapse"] = "latest"

    # Create network
    log_info("Creating network 'matrixnet'")
    handle_command(["podman", "network", "create", "matrixnet"])
    log_info("Network 'matrixnet' created")

    # Create folder for container generation scripts
    script_folder = os.path.join(os.getenv('HOME'), 'container-generation-scripts')
    os.makedirs(script_folder, exist_ok=True)

    # Set-up Postgres
    log_info("Pulling postgres image")
    handle_command(['podman', 'pull', f"docker.io/postgres:{config['postgres_tag']}"])
    log_info("Creating volume for postgres")
    handle_command(['podman', 'volume', 'create', 'postgres-data'])
    log_info("Generating text file for postgres")
    with open(os.path.join(script_folder, "postgres.txt"), "w") as file:
        file.write(f"""
podman run --name postgres \
    -e POSTGRES_PASSWORD={config['postgres_initial_password']} \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    --mount type=volume,src=postgres-data,dst=/var/lib/postgresql/data \
    --network=matrixnet \
    -c listen_addresses="'*'" \
    docker.io/postgres
"""
        )
    log_info("Text file for postgres generated")
    log_info(f"Creating systemd service for {name}")
    service_content = """[Unit]
    Description=Containerized postgres
    After=multi-user.target network-online.target

    [Service]
    ExecStart=/usr/bin/podman start -a postgres
    ExecStop=/usr/bin/podman stop -t 2 postgres
    Type=simple

    [Install]
    WantedBy=multi-user.target
    """
    service_path = f"/etc/systemd/system/postgres.service"
    with open(service_path, "w") as file:
        file.write(service_content)
    log_info(f"Systemd service for postgres created")

    # Reload systemd and enable service
    log_info("Reloading systemd daemon")
    handle_command(["systemctl", "daemon-reload"])
    log_info("Systemd daemon reloaded")

    log_info(f"Enabling and starting postgres")
    handle_command(["systemctl", "enable", "--now", "postgres"])
    log_info("Postgres has been setup")

    log_info("Installing bridges specified in config file")

    # Pull images, create volumes, generate text files, and systemd services
    for url, tag in image_dict.items():
        args = SimpleNamespace(image_url=url, tag=tag)
        add_bridge(args)

    log_info("First-time setup complete")

def add_bridge(args):
    image_url = args.image_url
    tag = args.tag
    postgres_pw = args.db_password

    config = get_config()

    # Check if user provided inputs; if not, ask for them
    if image_url is None:
        image_url = input("Please enter the image URL: ")
    if tag is None:
        tag = input("Please enter the image tag: ")
    if postgres_pw is None:
        postgres_pw = input("Please enter a password for the database to be used by the bridge: ")

    log_info(f"Adding bridge with image {image_url} and tag {tag}")

    # Pull the image
    log_info(f"Pulling image {image_url}:{tag}")
    handle_command(["podman", "pull", f"{image_url}:{tag}"])

    # Extract the name from the URL
    name = image_url.split('/')[-1]

    # Create the volume
    volume_name = f"{name}-data"
    log_info(f"Creating volume {volume_name}")
    handle_command(["podman", "volume", "create", volume_name])
    log_info(f"Volume {volume_name} created")

    # Generate text file
    run_command = [
        "podman run --name", name,
        f"--mount type=volume,src={volume_name},dst=/data",
        "--network=matrixnet",
        f"{image_url}"
    ]
    log_info(f"Generating text file for {name}")
    script_path = os.path.join(os.getenv('HOME'), 'container-generation-scripts')
    os.makedirs(script_path, exist_ok=True)
    with open(os.path.join(script_path, f"{name}.txt"), 'w') as file:
        file.write(' \\\n\t'.join(run_command))
    log_info(f"Text file for {name} generated")

    # Create systemd service
    log_info(f"Creating systemd service for {name}")
    service_content = f"""[Unit]
Description=Matrix server container running {name}
After=multi-user.target network-online.target

[Service]
ExecStart=/usr/bin/podman start -a {name}
ExecStop=/usr/bin/podman stop -t 2 {name}
Type=simple

[Install]
WantedBy=multi-user.target"""

    with open(f"/etc/systemd/system/{name}.service", 'w') as file:
        file.write(service_content)
    log_info(f"Systemd service for {name} created")

    # Reload systemd and enable service
    log_info("Reloading systemd daemon")
    subprocess.run(["systemctl", "daemon-reload"])
    log_info("Systemd daemon reloaded")

    log_info(f"Enabling service {name}")
    subprocess.run(["systemctl", "enable", name])
    log_info(f"Service {name} enabled")

    # Create postgres user
    log_info("Creating postgres user '{name}-pg'")
    handle_command(["podman", "exec", "-it", "-e", f"PGPASSWORD={config['postgres_initial_password']}", "postgres", "psql", "-U", "postgres", "-c", f"CREATE USER {name}-pg WITH PASSWORD '{postgres_pw}';"])
    log_info("Created postgres user for the new bridge")

    # Add the new bridge to the config file
    log_info("Adding new bridge to the configuration file")
    config_file = os.path.join(os.getenv('HOME'), 'matrixmgr.yaml')
    config['bridges'][image_url] = tag
    with open(config_file, 'w') as file:
        yaml.safe_dump(config, file)

    log_info("Added new bridge to the configuration file")
    log_info(f"You need to run the command contained in 'container-generation-scripts' (under your home directory) before the systemd service will automate the process. Customize this command according to the bridge documentation.")

def remove_bridge(args):
    image_url = args.image_url

    # Check if user provided inputs; if not, ask for them
    if image_url is None:
        image_url = input("Please enter the image URL: ")

    # Extract the name from the URL
    name = image_url.split('/')[-1]

    log_info(f"Stopping service {name}")
    subprocess.run(["systemctl", "stop", name])
    log_info(f"Service {name} stopped")

    log_info(f"Disabling service {name}")
    subprocess.run(["systemctl", "disable", name])
    log_info(f"Service {name} disabled")

    log_info(f"Deleting container generation file for {name}")
    script_folder = os.path.join(os.getenv('HOME'), 'container-generation-scripts')
    os.remove(os.path.join(script_folder, f"{name}.txt"))
    log_info(f"Removed file")

    log_info(f"Deleting container for {name}")
    handle_command(["podman", "container", "rm", name])
    log_info(f"Deleted container")

    log_info(f"Deleting image for {name}")
    handle_command(["podman", "rmi", image_url])
    log_info(f"Deleted image")

    # Remove bridge from the config file
    log_info("Removing bridge from the configuration file")
    config_file = os.path.join(os.getenv('HOME'), 'matrixmgr.yaml')
    with open(config_file, 'r') as file:
        config = yaml.safe_load(file)
    
    del config['bridges'][image_url]

    with open(config_file, 'w') as file:
        yaml.safe_dump(config, file)

    log_info(f"The volume for this bridge ({name}-data) was NOT deleted in order to preserve configuration. You can delete it manually with 'podman volume rm {name}-data'")

def main():
    parser = argparse.ArgumentParser(description="Manage a Matrix homeserver.")
    subparsers = parser.add_subparsers(dest='subcommand')

    # Subcommand for first-time setup
    parser_first_time_setup = subparsers.add_parser('first-time-setup')
    parser_first_time_setup.set_defaults(func=first_time_setup)

    # Subcommand for adding a bridge
    parser_add_bridge = subparsers.add_parser('add-bridge')
    parser_add_bridge.add_argument('--image-url', help='Image URL of the bridge')
    parser_add_bridge.add_argument('--tag', help='Tag for the bridge image')
    parser_add_bridge.add_argument('--db-password', help='Tag for the bridge image')
    parser_add_bridge.set_defaults(func=add_bridge)

    # Subcommand for removing a bridge
    parser_remove_bridge = subparsers.add_parser('remove-bridge')
    parser_remove_bridge.add_argument('--image-url', help='Image URL of the bridge')
    parser_remove_bridge.set_defaults(func=remove_bridge)

    args = parser.parse_args()
    if args.subcommand is None:
        parser.print_help()
    else:
        args.func(args)

if __name__ == "__main__":
    main()
